import { Component, OnInit } from '@angular/core';
import { Store } from '@ngrx/store';
import { Observable } from 'rxjs';

import * as CounterActions from '../store/counter.actions'; 

@Component({
  selector: 'app-my-counter',
  templateUrl: './my-counter.component.html',
  styleUrls: ['./my-counter.component.css']
})
export class MyCounterComponent implements OnInit {

  count$: Observable<number>;

  constructor(private store: Store<{ count: number }>) {
    // TODO: Connect `this.count$` stream to the current store `count` state
    this.count$ = store.select('count');
  }
  
  ngOnInit(): void {
  }

  increment() {
    // TODO: Dispatch an increment action
    this.store.dispatch(CounterActions.increment());
  }
 
  decrement() {
    // TODO: Dispatch a decrement action
    this.store.dispatch(CounterActions.decrement());
  }
 
  reset() {
    // TODO: Dispatch a reset action
    this.store.dispatch(CounterActions.reset());
  }

}
